'use strict';
var http = require('http');
var express = require('express');
var net = require('net');
var port = process.env.PORT || 1337;
var exec = require('child_process').exec;
var bodyParser = require('body-parser');
var translatePort = 11554;

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

var toBytesInt32 = function (num) {
    var ascii = Buffer.alloc(4);
    for (let i = 3; i >= 0; i--) {
        ascii[4-i-1] = ((num >> (8 * i)) & 255);
    }
    return ascii;
}

let child = exec('python translate_server.py ' + translatePort);
child.on('close', function () {
    console.log("TRANSLATION SERVER DIED!");
});
app.get('/', function (req, res) {
    res.status(200);
    res.send("Welcome to the terrible text summarizer API!");
});
app.all('/summarize', function (req, res) {
    let text = req.body["text"] || req.query["text"];
    if (!text) {
        let response = {};
        response["success"] = false;
        response["error"] = "No text provided.";
        res.status(400);
        res.send(JSON.stringify(response));
        return;
    }
    
    let sock = new net.Socket();
    sock.on('error', function (err) {
        console.log("Error in socket");
        console.log(err);
        try {
            let response = {};
            response["success"] = false;
            response["error"] = err.toString();
            res.status(500);
            res.send(JSON.stringify(response));
        } catch (err2) {
            console.log("Error transmitting the error :D");
            console.log(err2);
        }
    });
    sock.connect({
        "port": translatePort,
    }, function () {
        // Connected
        let length = -1;
        let totalChunk;
        let offset = 0;
        sock.on('data', function (data) {
            if (!Buffer.isBuffer(data)) { // Could be a string or a buffer
                data = Buffer.from(data, 'utf8');
            }
            if (length == -1) {
                length = data.readUIntBE(0, 4);
                console.log(length)
                data = data.slice(4); // Cut off first 4 bytes
                totalChunk = Buffer.alloc(length);
            }
            data.copy(totalChunk, offset);
            offset += data.length;
            if (offset >= length) {
                // We've got the translation! Sockets sock!
                let response = {};
                res.status(200);
                response["success"] = true;
                let trans = totalChunk.toString('utf8');
                
                response["translation"] = trans === '' ? text : trans;
                res.send(JSON.stringify(response));
                sock.end()
            }
        });
        let textLength = Buffer.byteLength(text, 'utf8');
        sock.write(toBytesInt32(textLength));
        sock.write(text, 'utf8', function () {
            // Well we wrote it; now we just gotta wait for the data to show up
        });
    });
});
app.listen(port, function () {
    console.log('I\'m listening now on port: '+port);
});