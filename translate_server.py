from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals

from sumy.parsers.html import HtmlParser
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer as Summarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words
import sys
import socket
import threading
import nltk

nltk.data.path.append('./nltk_data/')
LANGUAGE = "english"
SENTENCES_COUNT = 10

stemmer = Stemmer(LANGUAGE)
summarizer = Summarizer(stemmer)
summarizer.stop_words = get_stop_words(LANGUAGE)

class ConnectionThread(threading.Thread):
    def __init__(self, socket):
        super(ConnectionThread, self).__init__()
        self.sock = socket
    def mysend(self, msg):
        msg = msg.encode('utf8')
        theLength = len(msg)
        lengthBytes = int.to_bytes(theLength, 4, byteorder='big')
        totalsent = 0
        while totalsent < 4:
            sent = self.sock.send(lengthBytes[totalsent:])
            if sent == 0:
                raise RuntimeError("socket connection broken transmitting length")
            totalsent += sent
        totalsent = 0
        while totalsent < theLength:
            sent = self.sock.send(msg[totalsent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            totalsent = totalsent + sent

    def myreceive(self):
        chunks = []
        bytes_recd = 0
        lenChunk = b''
        while len(lenChunk) < 4:
            lenChunk += self.sock.recv(4-len(lenChunk))
        theLength = int.from_bytes(lenChunk, byteorder='big')
        print(lenChunk)
        print(theLength)
        while bytes_recd < theLength:
            chunk = self.sock.recv(min(theLength - bytes_recd, 2048))
            if chunk == '':
                raise RuntimeError("socket connection broken")
            chunks.append(chunk)
            bytes_recd = bytes_recd + len(chunk)
        return b''.join(chunks)
    def run(self):
        while True:
            #try:
                request = self.myreceive()
                parser = HtmlParser.from_string(request.decode('utf8'), "http://google.com/", Tokenizer(LANGUAGE))
                # 75 characters per sentence, min one sentence :)
                numSentences = max((len(request)/75), 1)
                sentences = [str(x) for x in summarizer(parser.document, numSentences)]
                result = ' '.join(sentences)
                self.mysend(result)
                print("Responded to request for %s(%d) with %s(%d); num sentences: %d" %
                      (request, len(request), result, len(result), numSentences))
            #except Exception as e:
            #    print("Well, my socket had a problem: %s" % (str(e),))
            #    break
        try:
            self.sock.close()
        except:
            pass
        
def main():
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    port = 0
    if len(sys.argv) < 2:
        port = 11554
    else:
        port = int(sys.argv[1])
    serversocket.bind(('', port))
    
    serversocket.listen(100)
    print("Listening on port %d for translation requests!" % (port,))
    while True:
        (clientsocket, address) = serversocket.accept()
        ct = ConnectionThread(clientsocket)
        ct.start()
        
if __name__ == '__main__':
    main()
